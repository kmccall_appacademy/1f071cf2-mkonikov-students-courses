class Student
  attr_accessor :first_name, :last_name, :courses
    def initialize(first_name, last_name)
      @first_name = first_name
      @last_name = last_name
      @courses = []
    end

    def name
      "#{@first_name} #{@last_name}"
    end

    def enroll(course)
      raise "Uh oh! This course conflicts with an existing one on your schedule." if has_conflict?(course)
      if !@courses.include?(course)
        @courses << course
        course.students << self
      end
    end

    def has_conflict?(course)
      return true if self.courses.any? { |el| course.conflicts_with?(el) }
      false
    end

    def course_load
      load = Hash.new(0)
      @courses.each { |el| load[el.department] += el.credits }
      load
    end

end
